<?php

use Illuminate\Http\Request;
use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1/')->group(function(){
    Route::post("/payment-confirmation","UserController@sendPaymentConfirm");

    Route::post("/user-confirmation","UserController@userConfirmation");

    Route::post("/get-started","UserController@create_account");
    Route::post("/get-topics","UserController@get_topics");

    Route::post("/set-push-id/{id}","UserController@push_id");

    Route::get("/get-subjects","UserController@getSubjects");
    Route::get("/get-random-playlist","UserController@getRandomPlaylist");
    Route::get("/get-random-videos","UserController@getRandomVideos");
    Route::get("/get-desks/{offset}","UserController@getDesks");

    Route::get("/get-playlist/{subject}/{offset}","UserController@getPlayList");

    Route::get("/get-playlist-audio/{subject}/{offset}","UserController@getPlayListAudio");

    Route::get("/get-video/{id}","UserController@getVideo");
    Route::get("/get-audio/{id}","UserController@getAudio");

    Route::post("/ask-question/{user}","UserController@askQuestion");
    Route::get("/view-question/{id}","UserController@viewQuestion");
    Route::post("/respond-to-question/{user}/{question}","UserController@respondToQuestion");

    Route::get("/questions/{file}","UserController@question_files");

    Route::get("/watch-video/videos/{file}/{user}/{code}","UserController@suggestedVideo_files");

    Route::get("/download-video/videos/{file}/{user}/{code}","UserController@download_video");

    Route::get("/thumbnails/{file}","UserController@view_thumbnail");
    Route::get("/ads/{file}","UserController@view_ad");


    Route::get("/watch-audio/audio/{file}/{user}/{code}","UserController@suggestedAudio_files");

    Route::get("/download-audio/audios/{file}/{user}/{code}","UserController@download_audio");


    Route::get('/search/{keyword}',"UserController@search");

//    Route::get('/test',function (){
//        $user=User::find(31);
//
//        return $user->refs;
//    });

});

//Route::get("/suggested-video","UserController@suggestedVideo_files");

Route::get("/download-files/{id}/{user}/{device}/{coupon}/{file}","UserController@download_files");


