<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/channel/{type}/{:id}', function(){
    return view('welcome');
});

Route::get('/thumbnail',function(){
    $videoPath=storage_path().'/videos/testing.mp4';

    VideoThumbnail::createThumbnail($videoPath, public_path().'/thumbnails', 'thmnb.png', 0, $width = 640, $height = 480);
});

Route::get('/adminLogin', 'AdminLoginController@showLoginForm');
Route::post('/adminLogin', 'AdminLoginController@login');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('adminLogin', 'AdminController');
Route::resource('/subject', 'SubjectController');
Route::resource('/video','VideoController');
Route::resource('/audio', 'AudioController');
Route::resource('/playlist', 'PlaylistController');
