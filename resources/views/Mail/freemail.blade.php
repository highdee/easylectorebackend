<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Easylector</title>
    <!-- Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Responsive, Multipurpose, one page, Portfolio, Business, Creative HTML Template">
    <meta name="description" content="Responsive HTML Business Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">

</head>
<body>


<div class="main-sectoin" style="font-family: 'Lato', sans-serif; max-width: 100%; width: 600px; margin: 0 auto;">
    <p>{{$text}}</p>
</div>

</body>
</html>
