<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@extends('layouts.assets')
	@section('content')
		<div class="app-content content">
	        <div class="content-overlay"></div>
	        <div class="content-wrapper">
	            <div class="content-header row">
	            </div>
	            <div class="content-body">
	                <!-- login page start -->
	                <section id="auth-login" class="row flexbox-container">
	                    <div class="col-xl-8 col-11">
	                        <div class="card bg-authentication mb-0">
	                            <div class="row m-0">
	                                <!-- left section-login -->
	                                <div class="col-md-6 col-12 px-0">
	                                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
	                                        <div class="card-header pb-1">
	                                            <div class="card-title">
	                                                <h4 class="text-center mb-2">Admin Login</h4>
	                                            </div>
	                                        </div>
	                                        @include('admin.message');
	                                        <div class="card-content">,
	                                            <div class="card-body">
	                                                <form action="/adminLogin" method="post">
	                                                	@csrf
	                                                    <div class="form-group mb-50">
	                                                        <label class="text-bold-600" for="exampleInputEmail1">Username</label>
	                                                        <input type="text" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" id="exampleInputEmail1" placeholder="Enter Username" name="username" value="{{ old('username') }}"  autofocus>
	                                                        @if ($errors->has('username'))
							                                    <span class="invalid-feedback" role="alert">
							                                        <strong>{{ $errors->first('username') }}</strong>
							                                    </span>
							                                @endif
	                                                    </div>
	                                                    <div class="form-group">
	                                                        <label class="text-bold-600" for="exampleInputPassword1">Password</label>
	                                                        <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  id="exampleInputPassword1" placeholder="Password">
	                                                        @if ($errors->has('password'))
							                                    <span class="invalid-feedback" role="alert">
							                                        <strong>{{ $errors->first('password') }}</strong>
							                                    </span>
							                                @endif
	                                                    </div>
	                                                    <!-- <div class="form-group">  
													        <input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>     
								                        	<label class="" for="remember">Remember me  </label>
								                        </div> -->

                    			                        <button type="submit" class="btn btn-primary glow w-100 position-relative">Login<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
	                                                </form>
	                                                <hr>
	                                                <!-- <div class="text-center"><small class="mr-25">Don't have an account?</small><a href="auth-register.html"><small>Sign up</small></a></div> -->
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- right section image -->
	                                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
	                                    <div class="card-content">
	                                        <img class="img-fluid" src="{{ asset('images/pages/login.png')}}" alt="branding logo">
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </section>
	                <!-- login page ends -->

	            </div>
	        </div>
    	</div>
	@endsection
</body>
</html>