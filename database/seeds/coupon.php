<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class coupon extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	DB::table('coupons')->insert([
    		'code'=>'94309589584',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'94309589522',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'943215895842',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'943095895334',
    		'used'=>0,
    		'user_id'=>0
    	]);

    	DB::table('coupons')->insert([
    		'code'=>'94309129584',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'94309221584',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'12309589584',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'94309589074',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'94309239584',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'94309589764',
    		'used'=>0,
    		'user_id'=>0
    	]);
    	DB::table('coupons')->insert([
    		'code'=>'94309589214',
    		'used'=>0,
    		'user_id'=>0
    	]);
    }
}
