<?php

use Illuminate\Database\Seeder;

class playlist extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('playlists')->insert([
            'subject'=>3,
            'title'=>'Cell Biology',
        ]);

        DB::table('playlists')->insert([
            'subject'=>3,
            'title'=>'Plasma',
        ]);


        DB::table('playlists')->insert([
            'subject'=>5,
            'title'=>'Forces',
        ]);

        DB::table('playlists')->insert([
            'subject'=>5,
            'title'=>'Optics',
        ]);
    }
}
