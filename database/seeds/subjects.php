<?php

use Illuminate\Database\Seeder;

class subjects extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('subjects')->insert([
            'title'=>'Mathematics'
        ]);
        DB::table('subjects')->insert([
            'title'=>'Gns'
        ]);
        DB::table('subjects')->insert([
            'title'=>'Biology'
        ]);
        DB::table('subjects')->insert([
            'title'=>'Chemistry'
        ]);
        DB::table('subjects')->insert([
            'title'=>'Physics'
        ]);
    }
}
