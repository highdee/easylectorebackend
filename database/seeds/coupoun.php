<?php

use Illuminate\Database\Seeder;

class coupoun extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	       for ($i=0; $i <1000 ; $i++) { 
	   				
	   			 DB::table('coupons')->insert([
		    		'code'=>strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10)),
		    		'used'=>0,
		    		'user_id'=>0
		    	]);

	       }
    }
}
