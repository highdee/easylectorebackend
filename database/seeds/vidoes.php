<?php

use Illuminate\Database\Seeder;

class vidoes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('videos')->insert(
            [
                'title'=>'An Example video',
                'description'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt',
                'playlist_id'=>1,
                'file'=>'videos/testing2.mp4',
                'duration'=>'00:40'

            ]
        );
    }
}
