<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Audios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('audios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('playlist_id');
            $table->string('title');
            $table->string('description');
            $table->string('file');
            $table->string('duration');
            $table->integer('views')->default(0);
            $table->integer('likes')->default(0);
            $table->integer('pickorder')->default(0);
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
