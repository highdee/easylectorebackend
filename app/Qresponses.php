<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qresponses extends Model
{
    //
    protected $appends=['date'];
//
//
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function getDateAttribute(){
        return $this->created_at->format('D, Y-m-d');
    }
}
