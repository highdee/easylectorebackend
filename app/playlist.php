<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class playlist extends Model
{
    //

    public function subject(){
        return $this->belongsTo('App\subject','subject','id');
    }

    public function videos(){
        return $this->hasMany('App\video','playlist_id','id')->orderBy('pickorder','asc');
    }

    public function audios(){
        $audios=$this->hasMany('App\audio','playlist_id','id')->orderBy('pickorder','asc');
        if($audios == null){
            $audios=[];
        }

        return $audios;
    }
}
