<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    //

    protected $appends=['user','subject','date','responsesLength'];

    public function getUserAttribute(){
        return $this->user()->first();
    }

    public function getResponsesLengthAttribute(){
        return $this->responsesLength()->count();
    }

    public function getSubjectAttribute(){
        return $this->subject()->first();
    }

    public function getDateAttribute(){
        return $this->created_at->format('D, Y-m-d');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function subject(){
        return $this->belongsTo('App\subject');
    }

    public function responses(){
        return $this->hasMany('App\Qresponses','question_id','id')->orderBy('id','desc');
    }
    public function responsesLength(){
        return $this->hasMany('App\Qresponses','question_id','id')->orderBy('id','desc');
    }
}
