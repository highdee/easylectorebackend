<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\playlist;


class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'playlistSubject' => 'required',
            'playlistTitle' => 'required|string',
            'playlistType' => 'required'
        ]);
        $data = $request->input();
        $s = new playlist();
        $s->subject = $data['playlistSubject'];
        $s->title = $data['playlistTitle'];
        $s->type = $data['playlistType'];
        if($s->save()){
            $msg = $s->title . ' Playlist Add Successfully';
            return redirect('/admin')->with('successMsg', $msg);
        }

        return redirect('/admin')->with('errorMsg', 'Playlist not Added Successfully, Try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
