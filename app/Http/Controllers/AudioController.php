<?php

namespace App\Http\Controllers;

use App\audio;
use Illuminate\Http\Request;

class AudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'audioName'=>'required',
            'selectSubject'=>'required',
            'selectAudioPlaylist'=>'required',
            'audioDescription'=>'required|string',
            'audiopickOrder'=>'required',
            'audioImage'=>'image|max:1999|nullable',       
        ]);

        if (!$request->hasfile('audioImage')) {
            return redirect()->back()->with('errorMsg', 'Please Select Audio Image ');   
        }
        if (!$request->hasfile('audio')) {
            return redirect()->back()->with('errorMsg', 'Please Select Audio file');
        }
            // audio image upload 
        $file=$request->file('audioImage');
        $fnx=$file->getClientOriginalName();//get img name with extension
        $fn = pathinfo($fnx, PATHINFO_FILENAME);//get image name
        $ext = $file->getClientOriginalExtension();//get extension
        $imgName = 'thumbnails/'.time().'_'.$ext;
        
        $format=['jpg','png','jpeg'];
        if (!in_array($ext, $format)) {
            return redirect()->back()->with('errorMsg', 'Invalid Image Format');
        }
            // audio file upload
        $f = $request->file('audio');
        $vnx = $f->getClientOriginalName();
        $vn=pathinfo($vnx, PATHINFO_FILENAME);
        $vext = $f->getClientOriginalExtension();
        $vname = 'audios/'.time().'.'.$vext;
        $fmat=['mp3'];
        if(!in_array($vext, $fmat)){
            return redirect()->back()->with('errorMsg', 'Invalid Audio Format');
        }

        $f->move(storage_path().'/audios', $vname);
        $file->move(storage_path(). '/thumbnails', $imgName);

        $data = $request->input();
        $lk = new audio();
        $lk->playlist_id = $data['selectAudioPlaylist'];
        $lk->title = $data['audioName'];
        $lk->description = $data['audioDescription'];
        $lk->pickorder = $data['audiopickOrder'];
        $lk->file = $vname;
        $lk->duration = 0;
        $lk->image = $imgName;



        if($lk->save()){
            return redirect()->back()->with('successMsg', 'Audio Uploaded Successfully');
        }
        return redirect()->back()->with('errorMsg', 'Error Uploading Audio, Please Try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
