<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\video;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'videoName'=>'required',
            'selectSubject'=>'required',
            'playlistId'=>'required',
            'videoDescription'=>'required|string',
            'videopickOrder'=>'required',
            'videoImage'=>'image|max:1999',
        ]);



        if (!$request->hasfile('videoImage')) {
            return redirect()->back()->with('errorMsg', 'Please Select Video Image ');   
        }
        if (!$request->hasfile('videoFile')) {
            return redirect()->back()->with('errorMsg', 'Please Select Video File');
        }
            // video image upload 
        $file=$request->file('videoImage');
        $fnx=$file->getClientOriginalName();//get img name with extension
        $fn = pathinfo($fnx, PATHINFO_FILENAME);//get image name
        $ext = $file->getClientOriginalExtension();//get extension
        $imgName = 'thumbnails/'.time().'_'.$ext;
        
        $format=['jpg','png','jpeg','JPG','PNG','JPEG'];
        if (!in_array($ext, $format)) {
            return redirect()->back()->with('errorMsg', 'Invalid Image Format');
        }
            // video file upload
        $f = $request->file('videoFile');
        $vnx = $f->getClientOriginalName();
        $vn=pathinfo($vnx, PATHINFO_FILENAME);
        $vext = $f->getClientOriginalExtension();
        $vname ='videos/'.time().'.'.$vext;
        $fmat=['mp4'];
        if(!in_array($vext, $fmat)){
            return redirect()->back()->with('errorMsg', 'Invalid Video Format , Use mp4');
        }

        $f->move(storage_path().'/videos', $vname);
        $file->move(storage_path(). '/thumbnails', $imgName);

        $data = $request->input();
        $lk = new video;
        $lk->playlist_id = $data['playlistId'];
        $lk->title = $data['videoName'];
        $lk->description = $data['videoDescription'];
        $lk->pickorder = $data['videopickOrder'];
        $lk->file = $vname;
        $lk->duration = 0;
        $lk->image = $imgName;



        if($lk->save()){
            return redirect()->back()->with('successMsg', 'Video Uploaded Successfully');
        }
        return redirect()->back()->with('errorMsg', 'Error Uploading Video, Please Try again');

        dd($request->file());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
