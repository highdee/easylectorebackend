<?php

namespace App\Http\Controllers;

use App\ad;
use App\audio;
use App\Mail\freemail;
use App\Mail\verificationEmail;
use App\playlist;
use App\Qresponses;
use App\question;
use App\subject;
use App\video;
use Illuminate\Http\Request;
use App\User;
use App\coupon;
use App\trials;
use App\book;
use Illuminate\Support\Facades\Mail;
use VideoThumbnail;
use App\Mail\confirm_payment;

class UserController extends Controller
{
    public function userConfirmation(Request $request){
        $user=User::where('device',$request->input('device'))->first();

        if(!$user){
            return response()->json(['code'=> -1 , 'msg'=>'Invalid coupon code'],402);
        }

        $code=coupon::where(['user_id'=>$user->id])->first();

        if(!$code){
            return response()->json(['code'=> -1 , 'msg'=>'Invalid coupon code'],402);
        }

        return response()->json(['code'=>1,'msg'=>'valid coupon','user'=>$user,'coupon'=>$code],200);
    }
    public function search($keyword){
        $videos=video::where('title','like','%'.$keyword.'%')->get();
        $audios=audio::where('title','like','%'.$keyword.'%')->get();
        $questions=question::where('content','like','%'.$keyword.'%')->get();
        $subjects=subject::where('title','like','%'.$keyword.'%')->with('playlist')->get();
        $playlist_aud=playlist::where('title','like','%'.$keyword.'%')->with('audios')->get();
        $playlist_vid=playlist::where('title','like','%'.$keyword.'%')->with('videos')->get();
        $playlists=[];

        foreach ($subjects as $key => $sub){
            foreach ($sub->playlist as $key => $sb){
                $playlists[]=$sb;
            }
        }


        foreach ($playlist_aud as $key => $pl){
            if(count($pl->audios) > 0){
                $pl->title='(audio) '.$pl->title;
                $playlists[]=$pl;
            }
        }

        foreach ($playlist_vid as $key => $pl){
            if(count($pl->videos) > 0){
                $pl->title='(video) '.$pl->title;
                $playlists[]=$pl;
            }
        }

        return [
            'videos'=>$videos,
            'audios'=>$audios,
            'questions'=>$questions,
            'playlists'=>$playlists,
            'keyword'=>$keyword
        ];
    }

    public function view_thumbnail($file){
        $videoPath=storage_path().'/thumbnails/'.$file;

        return response()->download($videoPath);
    }

    public function view_ad($file){
        $videoPath=storage_path().'/ads/'.$file;

        return response()->download($videoPath);
    }

    public function download_files($id,$user,$device,$coupon,$file){



        $user=User::where(['device'=>$device,'id'=>$user] )->first();

        if(!$user){
            return response()->json(['code'=>-1,'msg'=>'User bad request'],402);
        }


        $coupon=coupon::where(['code'=>$coupon,'used'=>"1",'user_id'=>$user->id])->first();

        if(!$coupon){
            return response()->json(['code'=>-1,'msg'=>'bad request'],402);
        }

        $doc=book::find($id);

        $audiobook=public_path('files/audio/'.$doc->audiobook);
        $ebook=public_path('files/docs/'.$doc->ebook);


       if($file == 1){
             return response()->download($audiobook);
       }

        return response()->download($ebook);
    }

    public function download_audio($file,$user,$code){
        $count=User::where(['id'=>$user,'device'=>$code])->count();


        if($count <=0 || $count>1){ return response()->json([],401);}

        $audio=storage_path('audios/'.$file);


        return response()->download($audio);
    }
    public function suggestedAudio_files($file,$user,$code){

        $count=User::where(['id'=>$user,'device'=>$code])->count();


        if($count <=0 || $count>1){ return response()->json([],401);}


        $mimetype = 'mime/type';
        header('Content-Type: audio/mp3');
        $handle = fopen(storage_path().'/audios/'.$file, "rb");
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, 1024*1024);
            echo $buffer;
            ob_flush();
            flush();
//                if ($retbytes) {
//                    $cnt += strlen($buffer);
//                }
        }
        $status = fclose($handle); ;

    }

    public function download_video($file,$user,$code){
        $count=User::where(['id'=>$user,'device'=>$code])->count();


        if($count <=0 || $count>1){ return response()->json([],401);}

        $video=storage_path('videos/'.$file);


        return response()->download($video);
    }
    public function question_files($file){

        $image=storage_path().'/questions/'.$file;


        return response()->download($image);
    }


    public function suggestedVideo_files($file,$user,$code){

        $count=User::where(['id'=>$user,'device'=>$code])->count();


        if($count <=0 || $count>1){ return response()->json([],401);}


            $mimetype = 'mime/type';
            header('Content-Type: video/mp4');
            $filename = storage_path().'/videos/'.$file;

//                $mime_type=Mime::from_extension($filename);
                return response()->file($filename,[
                    'Content-Type' => $mimetype,
                    'Content-Disposition' => 'inline; filename="Lesson-file"'
                ]);

//           $mimetype = 'mime/type';
//            header('Content-Type: video/mp4');
//            $handle = fopen(storage_path().'/videos/'.$file, "rb");
//            if ($handle === false) {
//                return false;
//            }
//            while (!feof($handle)) {
//                $buffer = fread($handle, 1024*1024);
//                echo $buffer;
//                // ob_flush();
//                // flush();
////                if ($retbytes) {
////                    $cnt += strlen($buffer);
////                }
//            }
//            ob_flush();
//                flush();
//            $status = fclose($handle);

    }


    public function get_topics(Request $request){
        $this->validate($request,[
            'code'=>'required',
            'device'=>'required',
            'user_id'=>'required'
        ]);


        $det=$request->input();

        $user=User::where(['device'=>$det['device'],'id'=>$det['user_id']])->first();

        if(!$user){
            return response()->json(['code'=>-1,'msg'=>'User bad request'],402);
        }

        $coupon=coupon::where(['code'=>$det['code'],'used'=>"1",'user_id'=>$user->id])->first();

        if(!$coupon){
            return response()->json(['code'=>-1,'msg'=>'bad request'],402);
        }

        $topics=book::all();

        return response()->json([
            'code'=>'1',
            'topics'=>$topics
        ],200);
    }

    public function getSubjects(){
        return subject::orderBy('title','asc')->get();
    }

    public function create_account(Request $request){
    	$this->validate($request,[
    		'name'=>'required',
    		'phone'=>"required",
    		'code'=>"required"
    	]);

        $det=$request->input();

        // return response()->json($det);

        $code=coupon::where(['code'=>$det['code']])->first();

        if(!isset($det['uuid'])){
            return response()->json(['code'=> -1 , 'msg'=>'Your device is not compatible with our platform,Please contact the admin'],404);
        }

        if(!$code){
            $trials=trials::where('phone',$det['phone'])->get();
            if(count($trials) >= 3){
                //send message to users
            }
            $trial=new trials();
            $trial->phone=$det['phone'];
            $trial->name=$det['name'];
            $trial->code=$det['code'];
            $trial->user_id=0;
            $trial->save();

            return response()->json(['code'=> -1 , 'msg'=>'invalid coupon code'],404);

        }
        elseif($code->user_id !=0 ){

            $user=User::where(['id'=>$code->user_id,'device'=>$det['uuid']])->first();

            if(!$user){
                //user already exist
                return response()->json(['code'=> -2 , 'msg'=>'Coupon code has already been used by another user'],402);
            }
            return response()->json(['code'=>1,'msg'=>'valid coupon','user'=>$user,'coupon'=>$code,'ss'=>''],200);
        }

        $user=new User();
        $user->name=$det['name'];
        $user->email='';
        $user->phone=$det['phone'];
        $user->device=$det['uuid'];

        if(isset($det['referral'])){
            $user->referral=$det['referral'];

            $ref=User::find($det['referral']);
            $count=count($ref->refs);

            if($ref && $ref->push_id != null){
                $this->sendNotification('Referral Link',$user->name.' registered using your link,Thank you. Complete 10 referrals and win a 1000',$ref->push_id);


                if($count > 0 && ($count % 10 )== 0){

                    $this->sendNotification('Referral Link','You have completed 10 referrals and you will receive your reward soon.',$ref->push_id);
                    $text=$ref->name." with user id ".$ref->id.' just completed 10 referrals';
                    Mail::to('highdee.ai@gmail.com')->send(new freemail(['text'=>$text])) ;



                }
            }
//            $user->save();

        }
        $user->save();

        $code->user_id=$user->id;
        $code->used=1;

        $code->save();

        return response()->json(['code'=>1,'msg'=>'valid coupon','user'=>$user,'coupon'=>$code],200);

    }

    public function sendNotification($title,$text,$push_id){
        $url = 'https://fcm.googleapis.com/fcm/send';
        $notification = array('title' =>$title, 'text' => $text);
        $headers = array(
            'Authorization: key=AAAAQrBhZ-k:APA91bFb5PCXXuFQ32z-ZxmiEGh84URIIh5Sz8nkvwyfTu-mBCuG4prxYETF0_cJNtYDsZqp3_PRr4QsfFLdEUhsaminwQ5v0S2pjhZIYM_GJHHxf9id2JAeuVHyyLsOJuaNSqb1Z9BM',
            'Content-type: Application/json'
        );

        $fields = array(
            'to' => $push_id,
            'data' => $message = array('message'=>'good'),
            'notification' => $notification
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);
    }

    public function askQuestion(Request $request,$user){
        $this->validate($request,[
            'subject'=>'required',
            'content'=>'required'
        ]);

        $question=new question();
        $question->user_id=$user;
        $question->subject_id=((int)$request->input('subject'));
        $question->content=$request->input('content');


        if($request->input('file')){
            $path='questions/'.date('YMdhis').$request->input('filename');
            $file=base64_decode($request->input('file'));
            $file=file_put_contents(storage_path().'/'.$path,$file);
            $question->file=$path;
        }

        $question->save();

        return $question;
    }
    public function viewQuestion($id){
        $question=question::where('id',$id)->with('responses','responses.user')->first();
        if($question){
            if($question->views == null){
                $question->views=0;
            }
            $question->views +=1;
            $question->save();
        }

        return $question;
    }
    public function getDesks($offset){
        $limit=9;
        $offsets=question::orderBy('id','desc')->skip($offset)->limit($limit)->get();

        return ['data'=>$offsets,'limit'=>$limit];
    }

    public function respondToQuestion(Request $request ,$user,$question){
        $this->validate($request,[
            'content'=>'required'
        ]);

        $User=User::find($user);
        if(!$User){
            return response()->json('',402);
        }

        $Question=question::find($question);
        if(!$Question){
            return response()->json('',402);
        }

        $response=new Qresponses();
        $response->user_id=$user;
        $response->question_id=$question;
        $response->content=$request->input('content');
        $response->save();

        return $response;
    }

    public function getRandomPlaylist(){
        $ply=playlist::where('id','>',0)->with('subject','videos','videos.playlist')->inRandomOrder()->limit(5)->get();

        return $ply;
    }
    public function getRandomVideos(){
        $vid=video::where('id','>',0)->with('playlist','playlist.subject')->inRandomOrder()->limit(5)->get();
        $ad=ad::where('active',1)->inRandomOrder()->limit(1)->first();

        return [$vid,$ad];
    }

    public function getPlayList($subject,$offset){
        $playlist=playlist::where(['subject'=>$subject,'type'=>1])->with('videos')->orderBy('title','asc')->skip($offset)->limit(5)->get();

        return $playlist;
    }

    public function getPlayListAudio($subject,$offset){
        $playlist=playlist::where(['subject'=>$subject,'type'=>2])->with('audios')->orderBy('title','asc')->skip($offset)->limit(5)->get();

        return $playlist;
    }

    public function getVideo($id){
        $video=video::where('id',$id)->with('playlist','playlist.videos')->first();
        $video->views+=1;
        $video->save();

        return $video;
    }

    public function getAudio($id){
        $audio=audio::where('id',$id)->with('playlist','playlist.audios')->first();
        $audio->views+=1;
        $audio->save();

        return $audio;
    }

    public function push_id(Request $request,$id){
        $user=User::find($id);

        $user->push_id=$request->input('device');
        $user->save();

        return json_encode('stored');
    }

    public function sendPaymentConfirm(Request $request){
        Mail::to('highdee.ai@gmail.com')->send(new confirm_payment(['sender'=>$request->input('sender'),'phone'=>$request->input('phone')]));

        return Response(['code'=>'1'],200);
    }
}
