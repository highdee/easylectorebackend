<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class audio extends Model
{
    //

    protected $table="audios";

    public function playlist(){
        return $this->belongsTo('App\playlist','playlist_id','id');
    }
}
